-- smazání všech záznamů z tabulek

CREATE or replace FUNCTION clean_tables() RETURNS void AS $$
declare
  l_stmt text;
begin
  select 'truncate ' || string_agg(format('%I.%I', schemaname, tablename) , ',')
    into l_stmt
  from pg_tables
  where schemaname in ('public');

  execute l_stmt || ' cascade';
end;
$$ LANGUAGE plpgsql;
select clean_tables();

-- reset sekvenci

CREATE or replace FUNCTION restart_sequences() RETURNS void AS $$
DECLARE
i TEXT;
BEGIN
 FOR i IN (SELECT column_default FROM information_schema.columns WHERE column_default SIMILAR TO 'nextval%')
  LOOP
         EXECUTE 'ALTER SEQUENCE'||' ' || substring(substring(i from '''[a-z_]*')from '[a-z_]+') || ' '||' RESTART 1;';
  END LOOP;
END $$ LANGUAGE plpgsql;
select restart_sequences();
-- konec resetu

-- konec mazání
-- mohli bchom použít i jednotlivé příkazy truncate na každo tabulku
INSERT INTO model (id_model, jmeno) VALUES (1, 'Veyron');
INSERT INTO model (id_model, jmeno) VALUES (2, 'Agera RS');
INSERT INTO model (id_model, jmeno) VALUES (3, 'Veneno');
INSERT INTO model (id_model, jmeno) VALUES (4, 'LaFerrari');
INSERT INTO model (id_model, jmeno) VALUES (5, 'P1');
INSERT INTO model (id_model, jmeno) VALUES (6, 'Valkyrie');
INSERT INTO model (id_model, jmeno) VALUES (7, 'Huayra BC');
INSERT INTO model (id_model, jmeno) VALUES (8, 'Sweptail');
INSERT INTO model (id_model, jmeno) VALUES (9, 'Mulliner Bacalar');
INSERT INTO model (id_model, jmeno) VALUES (10, '918 Spyder');
INSERT INTO model (id_model, jmeno) VALUES (11, 'Exelero');
INSERT INTO model (id_model, jmeno) VALUES (12, 'R8 GT');
INSERT INTO model (id_model, jmeno) VALUES (13, 'i8');
INSERT INTO model (id_model, jmeno) VALUES (14, 'LFA');
INSERT INTO model (id_model, jmeno) VALUES (15, 'NSX');
INSERT INTO model (id_model, jmeno) VALUES (16, 'Model S Plaid');
INSERT INTO model (id_model, jmeno) VALUES (17, 'XJ220');
INSERT INTO model (id_model, jmeno) VALUES (18, 'Evija');
INSERT INTO model (id_model, jmeno) VALUES (19, 'MC12');
INSERT INTO model (id_model, jmeno) VALUES (20, 'Carrera GT');

select setval(pg_get_serial_sequence('model', 'id_model'),20);

INSERT INTO znacka (id_znacka, jmeno) VALUES (1, 'Bugatti');
INSERT INTO znacka (id_znacka, jmeno) VALUES (2, 'Koenigsegg');
INSERT INTO znacka (id_znacka, jmeno) VALUES (3, 'Lamborghini');
INSERT INTO znacka (id_znacka, jmeno) VALUES (4, 'Ferrari');
INSERT INTO znacka (id_znacka, jmeno) VALUES (5, 'McLaren');
INSERT INTO znacka (id_znacka, jmeno) VALUES (6, 'Aston Martin');
INSERT INTO znacka (id_znacka, jmeno) VALUES (7, 'Pagani');
INSERT INTO znacka (id_znacka, jmeno) VALUES (8, 'Rolls-Royce');
INSERT INTO znacka (id_znacka, jmeno) VALUES (9, 'Bentley');
INSERT INTO znacka (id_znacka, jmeno) VALUES (10, 'Porsche');
INSERT INTO znacka (id_znacka, jmeno) VALUES (11, 'Mercedes-Maybach');
INSERT INTO znacka (id_znacka, jmeno) VALUES (12, 'Audi');
INSERT INTO znacka (id_znacka, jmeno) VALUES (13, 'BMW');
INSERT INTO znacka (id_znacka, jmeno) VALUES (14, 'Lexus');
INSERT INTO znacka (id_znacka, jmeno) VALUES (15, 'Tesla');
INSERT INTO znacka (id_znacka, jmeno) VALUES (16, 'Acura');
INSERT INTO znacka (id_znacka, jmeno) VALUES (17, 'Maserati');
INSERT INTO znacka (id_znacka, jmeno) VALUES (18, 'Lotus');
INSERT INTO znacka (id_znacka, jmeno) VALUES (19, 'Jaguar');

select setval(pg_get_serial_sequence('znacka', 'id_znacka'),19);

INSERT INTO motor (id_motor, objem, pocet_valcu, vykon) VALUES (1, 8,16,1001);
INSERT INTO motor (id_motor, objem, pocet_valcu, vykon) VALUES (2, 5,8,1160);
INSERT INTO motor (id_motor, objem, pocet_valcu, vykon) VALUES (3, 7,12,750);
INSERT INTO motor (id_motor, objem, pocet_valcu, vykon) VALUES (4, 6,12,949);
INSERT INTO motor (id_motor, objem, pocet_valcu, vykon) VALUES (5, 4,8,903);
INSERT INTO motor (id_motor, objem, pocet_valcu, vykon) VALUES (6, 7,12,1160);
INSERT INTO motor (id_motor, objem, pocet_valcu, vykon) VALUES (7, 6,12,789);
INSERT INTO motor (id_motor, objem, pocet_valcu, vykon) VALUES (8, 7,12,453);
INSERT INTO motor (id_motor, objem, pocet_valcu, vykon) VALUES (9, 6,12,650);
INSERT INTO motor (id_motor, objem, pocet_valcu, vykon) VALUES (10, 5,8,887);
INSERT INTO motor (id_motor, objem, pocet_valcu, vykon) VALUES (11, 6,12,700);
INSERT INTO motor (id_motor, objem, pocet_valcu, vykon) VALUES (12, 5,10,560);
INSERT INTO motor (id_motor, objem, pocet_valcu, vykon) VALUES (13, 2,3,369);
INSERT INTO motor (id_motor, objem, pocet_valcu, vykon) VALUES (14, 5,10,552);
INSERT INTO motor (id_motor, objem, pocet_valcu, vykon) VALUES (15, 4,6,573);
INSERT INTO motor (id_motor, objem, pocet_valcu, vykon) VALUES (16, 0,3,1000);
INSERT INTO motor (id_motor, objem, pocet_valcu, vykon) VALUES (17, 4,6,542);
INSERT INTO motor (id_motor, objem, pocet_valcu, vykon) VALUES (18, 0,4,1970);
INSERT INTO motor (id_motor, objem, pocet_valcu, vykon) VALUES (19, 6,12,623);
INSERT INTO motor (id_motor, objem, pocet_valcu, vykon) VALUES (20, 6,10,603);

select setval(pg_get_serial_sequence('motor', 'id_motor'),20);

insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (1, 18, 13, 3, 'MZ453XV', 2, 2019, 11.19);
insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (2, 18, 16, 14, 'ME323PW', 4, 2009, 27.53);
insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (3, 4, 3, 6, 'GT675KL', 3, 2018, 41.3);
insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (4, 5, 8, 8, 'UW887NX', 1, 2014, 12.21);
insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (5, 2, 19, 4, 'SW425MO', 3, 2023, 48.46);
insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (6, 6, 12, 8, 'OQ563AC', 2, 2009, 13.01);
insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (7, 6, 13, 9, 'LA532RN', 3, 2021, 11.15);
insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (8, 12, 4, 6, 'ZE612GS', 2, 2022, 34.12);
insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (9, 3, 15, 2, 'JG456VM', 4, 2018, 13.71);
insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (10, 18, 13, 3, 'LS956RT', 3, 2017, 21.69);
insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (11, 15, 1, 8, 'HU798IL', 3, 2022, 19.49);
insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (12, 20, 12, 13, 'PX010HQ', 1, 2017, 21.5);
insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (13, 20, 2, 3, 'UG456MY', 2, 2017, 20.57);
insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (14, 16, 4, 1, 'ZD572DI', 2, 2022, 31.05);
insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (15, 5, 14, 13, 'TQ338BD', 2, 2023, 48.53);
insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (16, 14, 1, 17, 'HE184BO', 4, 2013, 47.24);
insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (17, 2, 13, 14, 'LK538AM', 4, 2008, 19.4);
insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (18, 4, 6, 4, 'KW998KJ', 3, 2022, 19.14);
insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (19, 19, 2, 9, 'TU393DF', 4, 2008, 26.36);
insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (20, 8, 8, 2, 'FR235VZ', 2, 2023, 38.11);

select setval(pg_get_serial_sequence('auto', 'id_auto'),20);

insert into zamestnanec (id_zamestnanec, jmeno, prijmeni, plat) values (1, 'Jordan', 'MacGorman', 128308);
insert into zamestnanec (id_zamestnanec, jmeno, prijmeni, plat) values (2, 'Gabie', 'McIlmorie', 93721);
insert into zamestnanec (id_zamestnanec, jmeno, prijmeni, plat) values (3, 'Ellis', 'McFarlan', 91855);
insert into zamestnanec (id_zamestnanec, jmeno, prijmeni, plat) values (4, 'Sarene', 'Rapley', 80736);
insert into zamestnanec (id_zamestnanec, jmeno, prijmeni, plat) values (5, 'Worden', 'Eglaise', 78259);
insert into zamestnanec (id_zamestnanec, jmeno, prijmeni, plat) values (6, 'Taber', 'Maddy', 93496);
insert into zamestnanec (id_zamestnanec, jmeno, prijmeni, plat) values (7, 'Sorcha', 'Pilmoor', 66192);
insert into zamestnanec (id_zamestnanec, jmeno, prijmeni, plat) values (8, 'Traci', 'Ravenshaw', 53856);
insert into zamestnanec (id_zamestnanec, jmeno, prijmeni, plat) values (9, 'Charlotte', 'Hartup', 63249);
insert into zamestnanec (id_zamestnanec, jmeno, prijmeni, plat) values (10, 'Celestyna', 'Christensen', 98168);
insert into zamestnanec (id_zamestnanec, jmeno, prijmeni, plat) values (11, 'Catherina', 'Standell', 46598);
insert into zamestnanec (id_zamestnanec, jmeno, prijmeni, plat) values (12, 'Meggie', 'Bordone', 97854);
insert into zamestnanec (id_zamestnanec, jmeno, prijmeni, plat) values (13, 'Edi', 'Beswell', 52010);
insert into zamestnanec (id_zamestnanec, jmeno, prijmeni, plat) values (14, 'Thayne', 'Arnald', 85546);
insert into zamestnanec (id_zamestnanec, jmeno, prijmeni, plat) values (15, 'Millard', 'Menichillo', 63552);
insert into zamestnanec (id_zamestnanec, jmeno, prijmeni, plat) values (16, 'Gar', 'Ordish', 108386);
insert into zamestnanec (id_zamestnanec, jmeno, prijmeni, plat) values (17, 'Dennison', 'Bewshaw', 94872);
insert into zamestnanec (id_zamestnanec, jmeno, prijmeni, plat) values (18, 'Brittany', 'Fairrie', 124954);
insert into zamestnanec (id_zamestnanec, jmeno, prijmeni, plat) values (19, 'Lesley', 'McCaughey', 100006);
insert into zamestnanec (id_zamestnanec, jmeno, prijmeni, plat) values (20, 'Michelina', 'Cartan', 92483);

select setval(pg_get_serial_sequence('zamestnanec', 'id_zamestnanec'),20);

insert into asistent_jizd (id_zamestnanec, pohlavi) values (20, 'M');
insert into asistent_jizd (id_zamestnanec, pohlavi) values (11, 'M');
insert into asistent_jizd (id_zamestnanec, pohlavi) values (16, 'F');
insert into asistent_jizd (id_zamestnanec, pohlavi) values (13, 'M');
insert into asistent_jizd (id_zamestnanec, pohlavi) values (3, 'M');
insert into asistent_jizd (id_zamestnanec, pohlavi) values (5, 'F');
insert into asistent_jizd (id_zamestnanec, pohlavi) values (7, 'F');
insert into asistent_jizd (id_zamestnanec, pohlavi) values (1, 'F');
insert into asistent_jizd (id_zamestnanec, pohlavi) values (2, 'M');
insert into asistent_jizd (id_zamestnanec, pohlavi) values (14, 'F');
insert into asistent_jizd (id_zamestnanec, pohlavi) values (15, 'F');
insert into asistent_jizd (id_zamestnanec, pohlavi) values (4, 'F');

insert into vypujcitel (id_zamestnanec) values (16);
insert into vypujcitel (id_zamestnanec) values (14);
insert into vypujcitel (id_zamestnanec) values (2);
insert into vypujcitel (id_zamestnanec) values (19);
insert into vypujcitel (id_zamestnanec) values (9);
insert into vypujcitel (id_zamestnanec) values (8);
insert into vypujcitel (id_zamestnanec) values (15);
insert into vypujcitel (id_zamestnanec) values (7);
insert into vypujcitel (id_zamestnanec) values (3);
insert into vypujcitel (id_zamestnanec) values (20);
insert into vypujcitel (id_zamestnanec) values (6);
insert into vypujcitel (id_zamestnanec) values (11);
insert into vypujcitel (id_zamestnanec) values (12);


insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (1, '232677/3815', 'Fionnula', 'Lebbon');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (2, '988828/8052', 'Roxy', 'Losselyong');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (3, '836666/9397', 'Kevan', 'De La Salle');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (4, '622790/5148', 'Suzie', 'Keggins');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (5, '279655/2769', 'Archaimbaud', 'Heasley');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (6, '885144/9685', 'Gilberte', 'Chattock');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (7, '975128/0277', 'Willdon', 'Arsey');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (8, '032796/6863', 'Lenka', 'Burchfield');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (9, '706138/8254', 'Alessandra', 'Sprade');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (10, '920413/6050', 'Colas', 'Robke');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (11, '200787/6964', 'Estevan', 'Ffrench');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (12, '139328/6411', 'Heinrick', 'Dunks');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (13, '861088/3869', 'Lesley', 'Bowmer');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (14, '865583/6448', 'Lani', 'Tomala');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (15, '205212/2893', 'Merrie', 'Buncom');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (16, '852178/7098', 'Alvina', 'Have');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (17, '659054/5266', 'Corrinne', 'Ferfulle');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (18, '709357/7764', 'Terrye', 'Goadbie');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (19, '316274/5689', 'Georgi', 'Marishenko');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (20, '193872/9812', 'Zorana', 'Laherty');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (21, '020413/9812', 'Filip', 'Klokočník');
insert into zakaznik (id_zakaznik, rodne_cislo, jmeno, prijmeni) values (22, '020413/9812', 'Jakub', 'Klokočník');

select setval(pg_get_serial_sequence('zakaznik', 'id_zakaznik'),22);


insert into pojisteni (id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do) values (1, 1, 1, 192369, '6/22/2024');
insert into pojisteni (id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do) values (2, 2, 2, 120995, '6/3/2024');
insert into pojisteni (id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do) values (3, 3, 3, 199744, '7/12/2024');
insert into pojisteni (id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do) values (4, 4, 4, 175324, '6/10/2024');
insert into pojisteni (id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do) values (5, 5, 5, 105888, '6/9/2024');
insert into pojisteni (id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do) values (6, 6, 6, 144405, '10/17/2024');
insert into pojisteni (id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do) values (7, 7, 7, 193577, '10/14/2024');
insert into pojisteni (id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do) values (8, 8, 8, 178066, '8/8/2024');
insert into pojisteni (id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do) values (9, 9, 9, 112813, '10/26/2024');
insert into pojisteni (id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do) values (10, 10, 10, 165857, '9/6/2024');
insert into pojisteni (id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do) values (11, 11, 11, 195619, '7/1/2024');
insert into pojisteni (id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do) values (12, 12, 12, 157864, '1/15/2025');
insert into pojisteni (id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do) values (13, 13, 13, 175369, '6/23/2024');
insert into pojisteni (id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do) values (14, 14, 14, 175186, '12/16/2024');
insert into pojisteni (id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do) values (15, 15, 15, 187489, '7/19/2024');
insert into pojisteni (id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do) values (16, 16, 16, 171975, '11/21/2024');
insert into pojisteni (id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do) values (17, 17, 17, 105684, '1/7/2025');
insert into pojisteni (id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do) values (18, 18, 18, 183911, '10/9/2024');
insert into pojisteni (id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do) values (19, 19, 19, 167823, '5/6/2024');
insert into pojisteni (id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do) values (20, 20, 20, 157979, '1/13/2025');

insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (2, 8, 2, '4/1/2023', 113853.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 11, 3, '12/9/2022', 92188.6);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (4, 14, 4, '12/8/2022', 50211.6);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (5, 11, 5, '4/12/2023', 112278.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (6, 20, 6, '1/28/2023', 140165.3);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (7, 15, 7, '10/5/2022', 78936.1);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (8, 6, 8, '1/20/2023', 139753.4);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (9, 19, 9, '12/28/2022', 138191.2);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (10, 15, 10, '8/17/2022', 128084.1);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (11, 20, 11, '8/1/2022', 27588.3);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (12, 3, 12, '10/8/2022', 99224.6);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (13, 8, 13, '1/1/2023', 59312.0);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (14, 11, 14, '4/24/2023', 35066.1);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (15, 19, 15, '12/23/2022', 91429.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (15, 20, 15, '12/02/2022', 91429.5);

insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 8, 1, '8/22/2022', 113775.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 8, 2, '4/1/2023', 113853.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 11, 3, '12/9/2022', 92188.6);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 14, 4, '12/8/2022', 50211.6);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 11, 5, '4/12/2023', 112278.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 20, 6, '1/28/2023', 140165.3);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 15, 7, '10/5/2022', 78936.1);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 6, 8, '1/20/2023', 139753.4);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 19, 9, '12/28/2022', 138191.2);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 15, 10, '8/17/2022', 128084.1);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 20, 11, '8/1/2022', 27588.3);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 3, 12, '10/8/2022', 99224.6);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 8, 13, '1/1/2023', 59312.0);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 11, 14, '4/24/2023', 35066.1);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 19, 15, '12/23/2022', 91429.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 20, 16, '12/02/2022', 91429.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 20, 17, '12/02/2022', 91429.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 20, 18, '12/02/2022', 91429.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 20, 19, '12/02/2022', 91429.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 20, 20, '12/02/2022', 91429.5);

insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 8, 1, '8/22/2022', 113775.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 8, 2, '4/1/2023', 113853.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 14, 4, '12/8/2022', 50211.6);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 11, 5, '4/12/2023', 112278.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 20, 6, '1/28/2023', 140165.3);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 15, 7, '10/5/2022', 78936.1);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 6, 8, '1/20/2023', 139753.4);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 19, 9, '12/28/2022', 138191.2);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 15, 10, '8/17/2022', 128084.1);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 20, 11, '8/1/2022', 27588.3);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 3, 12, '10/8/2022', 99224.6);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 8, 13, '1/1/2023', 59312.0);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 11, 14, '4/24/2023', 35066.1);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 19, 15, '12/23/2022', 91429.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 20, 16, '12/02/2022', 91429.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 20, 17, '12/02/2022', 91429.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 20, 18, '12/02/2022', 91429.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 20, 19, '12/02/2022', 91429.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 20, 20, '12/02/2022', 91429.5);

insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (1, 8, 5, '8/22/2022', 113775.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (2, 8, 5, '4/1/2023', 113853.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (3, 14, 5, '12/8/2022', 50211.6);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (4, 11, 5, '4/12/2023', 112278.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (5, 20, 5, '1/28/2023', 140165.3);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (6, 15, 5, '10/5/2022', 78936.1);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (7, 6, 5, '1/20/2023', 139753.4);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (8, 19, 5, '12/28/2022', 138191.2);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (9, 15, 5, '8/17/2022', 128084.1);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (10, 20, 5, '8/1/2022', 27588.3);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (11, 3, 5, '10/8/2022', 99224.6);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (12, 8, 5, '1/1/2023', 59312.0);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (13, 11, 5, '4/24/2023', 35066.1);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (14, 19, 5, '12/23/2022', 91429.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (15, 20, 5, '12/02/2022', 91429.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (16, 20, 5, '12/02/2022', 91429.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (17, 20, 5, '12/02/2022', 91429.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (18, 20, 5, '12/02/2022', 91429.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (19, 20, 5, '12/02/2022', 91429.5);
insert into vypujcka (id_auto, id_zamestnanec, id_zakaznik, datum_do, najete_kilometry) values (20, 20, 5, '12/02/2022', 91429.5);

insert into zkusebni_jizda (id_zakaznik, id_auto, id_zamestnanec, datum) values (12, 11, 16, '9/22/2022');
insert into zkusebni_jizda (id_zakaznik, id_auto, id_zamestnanec, datum) values (4, 13, 16, '7/14/2022');
insert into zkusebni_jizda (id_zakaznik, id_auto, id_zamestnanec, datum) values (1, 13, 4, '12/29/2022');
insert into zkusebni_jizda (id_zakaznik, id_auto, id_zamestnanec, datum) values (19, 7, 11, '12/5/2022');
insert into zkusebni_jizda (id_zakaznik, id_auto, id_zamestnanec, datum) values (11, 17, 13, '1/8/2024');
insert into zkusebni_jizda (id_zakaznik, id_auto, id_zamestnanec, datum) values (3, 10, 1, '1/3/2024');
insert into zkusebni_jizda (id_zakaznik, id_auto, id_zamestnanec, datum) values (11, 8, 5, '1/16/2023');
insert into zkusebni_jizda (id_zakaznik, id_auto, id_zamestnanec, datum) values (17, 20, 20, '12/24/2022');
insert into zkusebni_jizda (id_zakaznik, id_auto, id_zamestnanec, datum) values (11, 17, 14, '7/23/2023');
insert into zkusebni_jizda (id_zakaznik, id_auto, id_zamestnanec, datum) values (2, 20, 7, '10/15/2023');
insert into zkusebni_jizda (id_zakaznik, id_auto, id_zamestnanec, datum) values (1, 15, 13, '8/13/2023');
insert into zkusebni_jizda (id_zakaznik, id_auto, id_zamestnanec, datum) values (18, 6, 4, '3/2/2023');
insert into zkusebni_jizda (id_zakaznik, id_auto, id_zamestnanec, datum) values (15, 16, 7, '1/19/2023');
insert into zkusebni_jizda (id_zakaznik, id_auto, id_zamestnanec, datum) values (2, 10, 11, '12/13/2022');
insert into zkusebni_jizda (id_zakaznik, id_auto, id_zamestnanec, datum) values (16, 19, 2, '7/1/2022');