-- zdroj: https://users.fit.cvut.cz/~hunkajir/dbs2/main.xml
-- odeberu pokud existuje funkce na oodebrání tabulek a sekvencí
DROP FUNCTION IF EXISTS remove_all();

-- vytvořím funkci která odebere tabulky a sekvence
-- chcete také umět psát PLSQL? Zapište si předmět BI-SQL ;-)
CREATE or replace FUNCTION remove_all() RETURNS void AS $$
DECLARE
    rec RECORD;
    cmd text;
BEGIN
    cmd := '';

    FOR rec IN SELECT
            'DROP SEQUENCE ' || quote_ident(n.nspname) || '.'
                || quote_ident(c.relname) || ' CASCADE;' AS name
        FROM
            pg_catalog.pg_class AS c
        LEFT JOIN
            pg_catalog.pg_namespace AS n
        ON
            n.oid = c.relnamespace
        WHERE
            relkind = 'S' AND
            n.nspname NOT IN ('pg_catalog', 'pg_toast') AND
            pg_catalog.pg_table_is_visible(c.oid)
    LOOP
        cmd := cmd || rec.name;
    END LOOP;

    FOR rec IN SELECT
            'DROP TABLE ' || quote_ident(n.nspname) || '.'
                || quote_ident(c.relname) || ' CASCADE;' AS name
        FROM
            pg_catalog.pg_class AS c
        LEFT JOIN
            pg_catalog.pg_namespace AS n
        ON
            n.oid = c.relnamespace WHERE relkind = 'r' AND
            n.nspname NOT IN ('pg_catalog', 'pg_toast') AND
            pg_catalog.pg_table_is_visible(c.oid)
    LOOP
        cmd := cmd || rec.name;
    END LOOP;

    EXECUTE cmd;
    RETURN;
END;
$$ LANGUAGE plpgsql;
-- zavolám funkci co odebere tabulky a sekvence - Mohl bych dropnout celé schéma a znovu jej vytvořit, použíjeme však PLSQL
select remove_all();


CREATE TABLE asistent_jizd (
    id_zamestnanec INTEGER NOT NULL,
    pohlavi VARCHAR(20) NOT NULL
);
ALTER TABLE asistent_jizd ADD CONSTRAINT pk_asistent_jizd PRIMARY KEY (id_zamestnanec);

CREATE TABLE auto (
    id_auto SERIAL NOT NULL,
    id_model INTEGER NOT NULL,
    id_motor INTEGER NOT NULL,
    id_znacka INTEGER NOT NULL,
    registracni_znacka VARCHAR(50) NOT NULL,
    pocet_mist INTEGER NOT NULL,
    rok_vyroby INTEGER NOT NULL,
    cena_za_km DOUBLE PRECISION
);
ALTER TABLE auto ADD CONSTRAINT pk_auto PRIMARY KEY (id_auto);

CREATE TABLE model (
    id_model SERIAL NOT NULL,
    jmeno VARCHAR(50) NOT NULL
);
ALTER TABLE model ADD CONSTRAINT pk_model PRIMARY KEY (id_model);

CREATE TABLE motor (
    id_motor SERIAL NOT NULL,
    objem INTEGER NOT NULL,
    pocet_valcu INTEGER NOT NULL,
    vykon INTEGER NOT NULL
);
ALTER TABLE motor ADD CONSTRAINT pk_motor PRIMARY KEY (id_motor);

CREATE TABLE pojisteni (
    id_pojisteni SERIAL NOT NULL,
    id_auto INTEGER NOT NULL,
    id_zamestnanec INTEGER NOT NULL,
    cislo_pojistky INTEGER NOT NULL,
    datum_do DATE NOT NULL
);
ALTER TABLE pojisteni ADD CONSTRAINT pk_pojisteni PRIMARY KEY (id_pojisteni);

CREATE TABLE vypujcitel (
    id_zamestnanec INTEGER NOT NULL
);
ALTER TABLE vypujcitel ADD CONSTRAINT pk_vypujcitel PRIMARY KEY (id_zamestnanec);

CREATE TABLE vypujcka (
    id_auto INTEGER NOT NULL,
    id_zamestnanec INTEGER NOT NULL,
    id_zakaznik INTEGER NOT NULL,
    datum_do DATE NOT NULL,
    najete_kilometry DOUBLE PRECISION NOT NULL
);
ALTER TABLE vypujcka ADD CONSTRAINT pk_vypujcka PRIMARY KEY (id_auto, id_zamestnanec, id_zakaznik);

CREATE TABLE zakaznik (
    id_zakaznik SERIAL NOT NULL,
    jmeno VARCHAR(50) NOT NULL,
    prijmeni VARCHAR(50) NOT NULL,
    rodne_cislo VARCHAR(30) NOT NULL
);
ALTER TABLE zakaznik ADD CONSTRAINT pk_zakaznik PRIMARY KEY (id_zakaznik);

CREATE TABLE zamestnanec (
    id_zamestnanec SERIAL NOT NULL,
    jmeno VARCHAR(50) NOT NULL,
    prijmeni VARCHAR(50) NOT NULL,
    plat INTEGER NOT NULL
);
ALTER TABLE zamestnanec ADD CONSTRAINT pk_zamestnanec PRIMARY KEY (id_zamestnanec);

CREATE TABLE zkusebni_jizda (
    id_zakaznik INTEGER NOT NULL,
    id_auto INTEGER NOT NULL,
    id_zamestnanec INTEGER NOT NULL,
    datum DATE NOT NULL
);
ALTER TABLE zkusebni_jizda ADD CONSTRAINT pk_zkusebni_jizda PRIMARY KEY (id_zakaznik, id_auto, id_zamestnanec);

CREATE TABLE znacka (
    id_znacka SERIAL NOT NULL,
    jmeno VARCHAR(50) NOT NULL
);
ALTER TABLE znacka ADD CONSTRAINT pk_znacka PRIMARY KEY (id_znacka);

ALTER TABLE asistent_jizd ADD CONSTRAINT fk_asistent_jizd_zamestnanec FOREIGN KEY (id_zamestnanec) REFERENCES zamestnanec (id_zamestnanec) ON DELETE CASCADE;

ALTER TABLE auto ADD CONSTRAINT fk_auto_model FOREIGN KEY (id_model) REFERENCES model (id_model) ON DELETE CASCADE;
ALTER TABLE auto ADD CONSTRAINT fk_auto_motor FOREIGN KEY (id_motor) REFERENCES motor (id_motor) ON DELETE CASCADE;
ALTER TABLE auto ADD CONSTRAINT fk_auto_znacka FOREIGN KEY (id_znacka) REFERENCES znacka (id_znacka) ON DELETE CASCADE;

ALTER TABLE pojisteni ADD CONSTRAINT fk_pojisteni_auto FOREIGN KEY (id_auto) REFERENCES auto (id_auto) ON DELETE CASCADE;
ALTER TABLE pojisteni ADD CONSTRAINT fk_pojisteni_zamestnanec FOREIGN KEY (id_zamestnanec) REFERENCES zamestnanec (id_zamestnanec) ON DELETE CASCADE;

ALTER TABLE vypujcitel ADD CONSTRAINT fk_vypujcitel_zamestnanec FOREIGN KEY (id_zamestnanec) REFERENCES zamestnanec (id_zamestnanec) ON DELETE CASCADE;

ALTER TABLE vypujcka ADD CONSTRAINT fk_vypujcka_auto FOREIGN KEY (id_auto) REFERENCES auto (id_auto) ON DELETE CASCADE;
ALTER TABLE vypujcka ADD CONSTRAINT fk_vypujcka_vypujcitel FOREIGN KEY (id_zamestnanec) REFERENCES vypujcitel (id_zamestnanec) ON DELETE CASCADE;
ALTER TABLE vypujcka ADD CONSTRAINT fk_vypujcka_zakaznik FOREIGN KEY (id_zakaznik) REFERENCES zakaznik (id_zakaznik) ON DELETE CASCADE;

ALTER TABLE zkusebni_jizda ADD CONSTRAINT fk_zkusebni_jizda_zakaznik FOREIGN KEY (id_zakaznik) REFERENCES zakaznik (id_zakaznik) ON DELETE CASCADE;
ALTER TABLE zkusebni_jizda ADD CONSTRAINT fk_zkusebni_jizda_auto FOREIGN KEY (id_auto) REFERENCES auto (id_auto) ON DELETE CASCADE;
ALTER TABLE zkusebni_jizda ADD CONSTRAINT fk_zkusebni_jizda_asistent_jizd FOREIGN KEY (id_zamestnanec) REFERENCES asistent_jizd (id_zamestnanec) ON DELETE CASCADE;

