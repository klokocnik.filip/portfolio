<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>BI-DBS.21 - Filip Klokočník (klokofil) </title>
<link rel="stylesheet" type="text/css" href="./sproject_html.css">
</head>
<body>
<script type="text/javascript">
var display=0;
function noComment(){
var comments=document.getElementsByName("comment");
if(display==1){
	set_to='block';
	display=0;
}
else{
	set_to='none';
	display=1;
};
for (i in comments){
	el=comments[i].style; 
	el.display=set_to;
}
return false;
}
</script><input type="button" value=" Vypnout / zapnout zobrazeni komentaru" onClick="return noComment();"><h3 class="course">BI-DBS.21 -            Databázové systémy,            B222</h3>
<h3 class="author">Filip Klokočník ( klokofil )</h3>
<h4 class="author"><a href="mailto:klokocnik.filip@gmail.com">klokocnik.filip@gmail.com</a></h4>
<p class="declaration">I declare that I have developed my semestral work independently. All the sources I used are listed in the section References.</p>
<h1>Půjčovna luxusních a sportovních aut</h1>
<h2>Popis</h2>
<p>Každý z nás se někdy chtěl projet v nějakém luxusním, nebo sportovním **autě**. Půjčovna DreamCar tuto možnost našim **zákazníkům** zprostředkovává na 1-3 dny. </p>
<p>U aut je podstatné znát **motor**(cheme  evidovat objem, výkon,  počet válců), **značku**(jméno značky), **model**(jméno modelu), rok výroby, počet míst, registrační značku a cenu za kilometr.</p>
<p>Předtím než si naši zákazníci auto vypůjčí, můžou si zvolené auto vyzkoušet na závodním okruhu, kde je bude instruovat náš **zaměstnanec**, který dříve profesionálně závodil(evidujeme pohlaví, aby si každý zákazník/zákaznice mohl vybrat co jim je příjemnější). **Zkušební jízda** trvá 45 minut a každý zákazník si může vyzkoušet libovolné množství aut.</p>
<p>Každé auto má **pojištění**(evidujeme číslo pojistky a do kdy pojistka platí), takže se zákazníci nemusí bát, že budou muset zaplatit za případné škody. Po vypršení období, ve kterém mohl zákazník v autě jezdit nám auto vrátí v předem domluvený čas.</p>
<p>Naše auta jsou rozdělená do 2 kategorií. Buď se může jednat o závodní auto, nebo o luxusní auto, ve kterém je důležitější pohodlí a požitek z obyčejné jízdy. Při vypůjčení zaměstnanec zaeviduje aktuální stav tachometru. V případě, že by zákazník překročil limit km pro jeho dané časové období, musí další najeté kilometry doplatit. Cenu za kilometr u daného auta mu sdělí zaměstnanec, který auto vypůjčuje. </p>
<p></p>
<h2>Conceptual schema</h2>
<img src="conceptual_schema.png" alt="
			Zde má být obrázek vašeho konceptuálního modelu.
               ">
<h3>Diskuze smyček</h3>
<p>Auto-Vypujcka-Vypujcitel-Zamestnanec-Asistent jizd-Zkusebni jizda</p>
<p>Auto mohlo být vypůjčeno stejným zaměstnancem se kterým byla provedena zkušební jízda ( to není problém, jelikož zaměstnanec může být jak asisten, tak vypujcitel )</p>
<p>Auto-Vypujcka-Zakaznik-Zkusebni jizda</p>
<p>Zákazník si vypůjčil auto, se kterým absolvoval zkušební jízdu. To také není žádný problém. Naopak to firma doporučuje kvůli spokojenosti zákazníka. </p>
<p>Zakaznik-Vypujcka-Vypujcitel-Zamestnanec-Asistent jizd-zkusebni jizda</p>
<p>Zákazníkovi vypůjčil auto stejný člověk, který s ním absolvoval zkušební jízdu. To je také v pořádku.</p>
<h2>Relační schema</h2>
<img src="relational_schema.png" alt="
			Zde má být obrázek vašeho relačního modelu.
               "><h2>Dotazy</h2>
<ol>
<li>
<p> Kód dotazu: <b><a name="D3">D3</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Auta s 8 válcovým motorem.</p>
<p class="ra">auto&lt;auto.id_motor=motor.id_motor]motor(pocet_valcu = 8)</p>
<p class="sql">select auto.* 
from auto join ( select * from motor where pocet_valcu=8 )R1 on auto.id_motor = R1.id_motor;

select auto.* from motor natural join auto where pocet_valcu=8 ;

select * from auto
except 
select auto.* from motor natural join auto where pocet_valcu!=8</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D4">D4</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Vyber auta, která byla vypůjčena pouze Archaimbaud Heasley
</p>
<p class="ra">{ {auto*vypujcka*zakaznik(jmeno = 'Archaimbaud' ∧ prijmeni = 'Heasley')}[id_auto, id_model, id_znacka, id_motor, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km] }
\
{ {auto*vypujcka*zakaznik(jmeno != 'Archaimbaud' ∨ prijmeni != 'Heasley')}[id_auto, id_model, id_znacka, id_motor, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km] }
</p>
<p class="sql">select auto.* from auto natural join vypujcka natural join ( select * from zakaznik where jmeno = 'Archaimbaud' and prijmeni='Heasley')R1 

except

select auto.* from auto natural join vypujcka natural join ( select * from zakaznik where jmeno != 'Archaimbaud' or prijmeni!='Heasley')R2 </p>
</li>
<li>
<p> Kód dotazu: <b><a name="D2">D2</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Auta, která mají pojištění do 6/22/2024</p>
<p class="ra">auto&lt;*pojisteni(datum_do = '6/22/2024')</p>
<p class="sql">select auto.* from auto natural join ( select * from pojisteni where datum_do = '6/22/2024')R1</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D5">D5</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Vyber zákazníka, který si vypůjčil všechna auta.</p>
<p class="ra">zakaznik * { vypujcka[id_zakaznik, id_auto] ÷ auto[id_auto] }
</p>
<p class="sql">select * 
from zakaznik
where ( select count(distinct id_auto) from vypujcka where vypujcka.id_zakaznik=zakaznik.id_zakaznik) = (select count(id_auto) from auto)</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D6">D6</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Všechna pojištění zaměstnanců, která vyprší v prosinci roku 2024</p>
<p class="ra">zamestnanec*&gt;pojisteni(datum_do &lt; '31.12.2024' ∧ datum_do &gt; '1.12.2024')
</p>
<p class="sql">select id_pojisteni, id_auto, id_zamestnanec, cislo_pojistky, datum_do from zamestnanec natural join ( select * from pojisteni where datum_do &lt; '12/31/2024' and datum_do &gt; '12/1/2024' )R2</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D7">D7</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Asistenti jízd, kteří asistoval u zkušební jízdy s autem značky Bentley</p>
<p class="ra">asistent_jizd*zkusebni_jizda*{auto*znacka(jmeno='Bentley')}</p>
<p class="sql">SELECT DISTINCT *
FROM ASISTENT_JIZD
NATURAL JOIN ZKUSEBNI_JIZDA
NATURAL JOIN (
    SELECT DISTINCT *
    FROM AUTO
    NATURAL JOIN (
        SELECT DISTINCT *
        FROM ZNACKA
        WHERE jmeno = 'Bentley'
    ) R1
) R2;</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D8">D8</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Vypujcky, které jsou do 4/24/2023</p>
<p class="ra">vypujcka(datum_do = '4/24/2023')</p>
<p class="sql">select * from vypujcka where datum_do = '4/24/2023' </p>
</li>
<li>
<p> Kód dotazu: <b><a name="D9">D9</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Zaměstnanci s platem vyšším než 100 000 Kč.</p>
<p class="ra">zamestnanec(plat &gt; 100000)</p>
<p class="sql">SELECT DISTINCT *
FROM ZAMESTNANEC
WHERE plat &gt; 100000;</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D10">D10</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Asistent jízd s nejvyšším platem.</p>
<p class="sql">select max(plat) 
from asistent_jizd natural join zamestnanec
</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D11">D11</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Vypiš všechna auta, která nikdy nebyla na zkusebni jizde.</p>
<p class="ra">auto \ {auto&lt;*zkusebni_jizda}</p>
<p class="sql">select * from auto
except 
select auto.* from auto natural join zkusebni_jizda</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D12">D12</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Pro všechna auta, která byla vypůjčena více než dvakrát a zaroven byla vypujcena max do 2022-12-27  vypiš kolikrát bylo vypůjčeno.</p>
<p class="sql">select count(id_auto) as pocetVypujceni, id_auto
from vypujcka
where datum_do &lt; '2022-12-28'
group by (id_auto)
having count(id_auto) &gt; 2
order by (id_auto) asc</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D13">D13</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Kontrola výsledku dotazu D1 ( Vyber zákazníka, který si vypůjčil všechny auta )</p>
<p class="sql">select * from auto

except

select auto.* from
vypujcka natural join auto 
where id_zakaznik=5
</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D14">D14</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Vyber všechny zamestnance, kteří jsou zároveň vypůjčitelé i asistenti jízd</p>
<p class="sql">select zamestnanec.* from
zamestnanec natural join asistent_jizd
intersect
select zamestnanec.* from
zamestnanec natural join vypujcitel

</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D15">D15</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Vyber všechna auta, která mají značku bentley a všechna auta, která mají 12 válcový motor.
auto&lt;*znacka(jmeno='Bentley');
</p>
<p class="ra">{auto&lt;*motor(pocet_valcu=12)}
∪
{auto&lt;*znacka(jmeno='Bentley')}

</p>
<p class="sql">select auto.* from auto natural join (select * from znacka where jmeno='Bentley' ) R1

union

select auto.* from auto natural join (select * from motor where pocet_valcu= 12 ) R2
order by id_auto</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D16">D16</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Vypiš všechny zaměstnance spolu s danou výpujckou, kteří vypůjčili auto s nejdražší cenou za kilometr</p>
<p class="sql">select * from zamestnanec cross join vypujcka where zamestnanec.id_zamestnanec=vypujcka.id_zamestnanec and vypujcka.id_auto=( select id_auto from auto where cena_za_km=(select max(cena_za_km) from auto) )</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D17">D17</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>U každého zaměstnance vypocitej pocet vypujcek, které udělal</p>
<p class="sql">select *, (select count(*) from vypujcka where vypujcitel.id_zamestnanec=id_zamestnanec) as pocet_vypujcenych_aut from vypujcitel order by (id_zamestnanec)</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D18">D18</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Vyber všechny vypujcitele, kteří ještě nevypůjčili auto</p>
<p class="sql">select * from vypujcitel where not exists ( select * from vypujcka where id_zamestnanec=vypujcitel.id_zamestnanec)</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D19">D19</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Vypiš všechny zakazniky a k nim auta, které si půjčili.</p>
<p class="sql">select * from zakaznik left join vypujcka on zakaznik.id_zakaznik=vypujcka.id_zakaznik
order by (zakaznik.id_zakaznik)</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D20">D20</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Vypis vsechny zaznamy o vypujcce, včetně zakazniku, kteří si žádné auto nevypucili a aut, která nebyla vypůjčena.</p>
<p class="sql">begin;
insert into auto (id_auto, id_model, id_motor, id_znacka, registracni_znacka, pocet_mist, rok_vyroby, cena_za_km) values (21, 8, 3, 2, 'FR235VZ', 2, 2000, 38.11);
select * from vypujcka full join zakaznik on vypujcka.id_zakaznik = zakaznik.id_zakaznik full join auto on vypujcka.id_auto = auto.id_auto;
rollback;</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D21">D21</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Vytvoř pohled aut, která byla vypůjčena více než 2x</p>
<p class="sql">create or replace view stastnaAuta as
select * from auto 
where ( select count(*) from vypujcka where vypujcka.id_auto=auto.id_auto) &gt; 2;
</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D22">D22</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Vyber auta z pohledu stastnaAuta, která mají motor o větším objemu než 4l </p>
<p class="sql">select stastnaAuta.* from stastnaAuta natural join ( select * from motor where objem &gt; 4)R1</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D23">D23</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Vyber zakazniky, kteří  jeli zkusebni jizdu 1/16/2023</p>
<p class="sql">select zakaznik.* from zakaznik natural join ( select * from zkusebni_jizda where datum = '1/16/2023')R1</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D24">D24</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Vložte ještě jednou zaměstnance Jordan MacGormana </p>
<p class="sql">begin;

select * from zamestnanec;
insert into zamestnanec(jmeno,prijmeni,plat)
select jmeno,prijmeni,plat from zamestnanec where jmeno='Jordan' and prijmeni ='MacGorman';
select * from zamestnanec

rollback;</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D25">D25</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Smažte všechny auta porsche</p>
<p class="sql">begin;

select * from auto;
delete from auto where id_auto in ( select id_auto from auto natural join ( select * from znacka where jmeno='Bentley')R1 );
select * from auto;

rollback;</p>
</li>
<li>
<p> Kód dotazu: <b><a name="D26">D26</a></b><a href="#tabulka_pokryti"> [Tabulka pokryti kategorií SQL příkazů]</a></p>
<p>Všem zaměstnancům se díky inflaci zvedá plat o 5%.</p>
<p class="sql">begin;

select plat from zamestnanec
order by plat;

update zamestnanec set plat = plat * 1.05
where id_zamestnanec in (
select id_zamestnanec from zamestnanec);

select plat from zamestnanec
order by plat;
rollback;</p>
</li>
</ol>
<h2><a name="tabulka_pokryti"> Tabulka pokrytí kategorií SQL příkazů </a></h2>
<table><tbody>
<tr>
<th> Kategorie </th>
<th> Kódy porývajících dotazů</th>
<th> Charekteristika kategorie příkazu</th>
</tr>
<tr>
<td>A</td>
<td>
<a href="#D2">D2</a> <a href="#D6">D6</a> <a href="#D7">D7</a> <a href="#D10">D10</a> <a href="#D11">D11</a> <a href="#D13">D13</a> <a href="#D14">D14</a> <a href="#D15">D15</a> <a href="#D19">D19</a> <a href="#D20">D20</a> <a href="#D22">D22</a> <a href="#D23">D23</a> <a href="#D25">D25</a> </td>
<td>A - Pozitivní dotaz nad spojením alespoň dvou tabulek</td>
</tr>
<tr>
<td>AR</td>
<td>
<a href="#D2">D2</a> <a href="#D6">D6</a> <a href="#D7">D7</a> <a href="#D11">D11</a> <a href="#D15">D15</a> </td>
<td>A (RA) - Pozitivní dotaz nad spojením alespoň dvou tabulek</td>
</tr>
<tr>
<td>B</td>
<td>
<a href="#D11">D11</a> </td>
<td>B - Negativní dotaz nad spojením alespoň dvou tabulek</td>
</tr>
<tr>
<td>C</td>
<td>
<a href="#D4">D4</a> </td>
<td>C - Vyber ty, kteří mají vztah POUZE k ...</td>
</tr>
<tr>
<td>D1</td>
<td>
<a href="#D5">D5</a> </td>
<td>D1 - Vyber ty, kteří/které jsou ve vztahu se všemi - dotaz s univerzální kvantifikací</td>
</tr>
<tr>
<td>D2</td>
<td>
<a href="#D13">D13</a> </td>
<td>D2 - Kontrola výsledku dotazu z kategorie D1</td>
</tr>
<tr>
<td>F1</td>
<td>
<a href="#D3">D3</a> </td>
<td>F1 - JOIN ON</td>
</tr>
<tr>
<td>F2</td>
<td>
<a href="#D3">D3</a> <a href="#D4">D4</a> <a href="#D2">D2</a> <a href="#D6">D6</a> <a href="#D7">D7</a> <a href="#D10">D10</a> <a href="#D11">D11</a> <a href="#D13">D13</a> <a href="#D14">D14</a> <a href="#D15">D15</a> <a href="#D22">D22</a> <a href="#D23">D23</a> <a href="#D25">D25</a> </td>
<td>F2 - NATURAL JOIN|JOIN USING</td>
</tr>
<tr>
<td>F2R</td>
<td>
<a href="#D3">D3</a> <a href="#D4">D4</a> <a href="#D2">D2</a> <a href="#D6">D6</a> <a href="#D7">D7</a> <a href="#D11">D11</a> <a href="#D15">D15</a> </td>
<td>F2 (RA) - NATURAL JOIN|JOIN USING</td>
</tr>
<tr>
<td>F3</td>
<td>
<a href="#D16">D16</a> </td>
<td>F3 - CROSS JOIN</td>
</tr>
<tr>
<td>F4</td>
<td>
<a href="#D19">D19</a> </td>
<td>F4 - LEFT|RIGHT OUTER JOIN</td>
</tr>
<tr>
<td>F5</td>
<td>
<a href="#D20">D20</a> </td>
<td>F5 - FULL (OUTER) JOIN</td>
</tr>
<tr>
<td>G1</td>
<td>
<a href="#D5">D5</a> <a href="#D16">D16</a> <a href="#D18">D18</a> <a href="#D21">D21</a> <a href="#D25">D25</a> <a href="#D26">D26</a> </td>
<td>G1 - Vnořený dotaz v klauzuli WHERE</td>
</tr>
<tr>
<td>G1R</td>
<td>
<a href="#D5">D5</a> </td>
<td>G1 (RA) - Vnořený dotaz v klauzuli WHERE</td>
</tr>
<tr>
<td>G2</td>
<td>
<a href="#D3">D3</a> </td>
<td>G2 - Vnořený dotaz v klauzuli FROM</td>
</tr>
<tr>
<td>G3</td>
<td>
<a href="#D17">D17</a> </td>
<td>G3 - Vnořený dotaz v klauzuli SELECT</td>
</tr>
<tr>
<td>G4</td>
<td>
<a href="#D18">D18</a> </td>
<td>G4 - Vztažený vnořený dotaz (EXISTS, NOT EXISTS)</td>
</tr>
<tr>
<td>H1</td>
<td>
<a href="#D15">D15</a> </td>
<td>H1 - Množinové sjednocení - UNION</td>
</tr>
<tr>
<td>H2</td>
<td>
<a href="#D3">D3</a> <a href="#D4">D4</a> <a href="#D11">D11</a> <a href="#D13">D13</a> </td>
<td>H2 - Množinový rozdíl - MINUS nebo EXCEPT</td>
</tr>
<tr>
<td>H2R</td>
<td>
<a href="#D3">D3</a> <a href="#D4">D4</a> <a href="#D11">D11</a> </td>
<td>H2 (RA) - Množinový rozdíl - MINUS nebo EXCEPT</td>
</tr>
<tr>
<td>H3</td>
<td>
<a href="#D14">D14</a> </td>
<td>H3 - Množinový průnik - INTERSECT</td>
</tr>
<tr>
<td>I1</td>
<td>
<a href="#D5">D5</a> <a href="#D10">D10</a> <a href="#D12">D12</a> <a href="#D16">D16</a> <a href="#D17">D17</a> <a href="#D21">D21</a> </td>
<td>I1 - Agregační funkce (count|sum|min|max|avg)</td>
</tr>
<tr>
<td>I1R</td>
<td>
<a href="#D5">D5</a> </td>
<td>I1 (RA) - Agregační funkce (count|sum|min|max|avg)</td>
</tr>
<tr>
<td>I2</td>
<td>
<a href="#D12">D12</a> </td>
<td>I2 - Agregační funkce nad seskupenými řádky - GROUP BY (HAVING)</td>
</tr>
<tr>
<td>J</td>
<td>
<a href="#D3">D3</a> </td>
<td>J - Stejný dotaz ve třech různých formulacích SQL</td>
</tr>
<tr>
<td>K</td>
<td>
<a href="#D12">D12</a> </td>
<td>K - Všechny klauzule v 1 dotazu - SELECT FROM WHERE GROUP BY HAVING ORDER BY</td>
</tr>
<tr>
<td>L</td>
<td>
<a href="#D21">D21</a> </td>
<td>L - VIEW</td>
</tr>
<tr>
<td>M</td>
<td>
<a href="#D22">D22</a> </td>
<td>M - Dotaz nad pohledem</td>
</tr>
<tr>
<td>N</td>
<td>
<a href="#D24">D24</a> </td>
<td>N - INSERT, který vloží do některé tabulky množinu řádků, které jsou vybrány dotazem z vybraných tabulek (příkaz INSERT, ve kterém je klauzule VALUES nahrazena vnořeným poddotazem.</td>
</tr>
<tr>
<td>O</td>
<td>
<a href="#D26">D26</a> </td>
<td>O - UPDATE s vnořeným SELECT příkazem</td>
</tr>
<tr>
<td>P</td>
<td>
<a href="#D25">D25</a> </td>
<td>P - DELETE s vnořeným SELECT příkazem</td>
</tr>
</tbody></table>
<h2>Scripts</h2>
<p>
                <a href="./create.sql">create.sql</a>
            </p>
<p>
                <a href="./insert.sql">insert.sql</a>
            </p>
<h2>Zdroje</h2>
<p>[2] Hunka, Jiří: .Vzorová semestrální práce [online]. FIT ČVUT, 2021, [cit. 13.5.2023]. Dostupné z: https://users.fit.cvut.cz/~hunkajir/dbs2/main.xml</p>
<p></p>
</body>
</html>
